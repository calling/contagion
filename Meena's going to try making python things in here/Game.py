# A game of hand-washing
# Meena Vempaty
# Contagion Ninja Team

import os, sys, random, time, math
import Leap
import pygame
from pygame.locals import *
from mingus.core import notes, chords
from mingus.containers import *
from mingus.midi import fluidsynth
import mingus.core.scales as scales

if not pygame.font: print('Warning, fonts disabled')
if not pygame.mixer: print('Warning, sound disabled')

controller = Leap.Controller()

pygame.init() 
window = pygame.display.set_mode((800, 600)) 
pygame.display.set_caption('Contagion Project') 
screen = pygame.display.get_surface()
font = pygame.font.Font("Future Millennium.ttf", 30)

SF2 = 'soundfont.sf2'
if not fluidsynth.init(SF2, "alsa"):
    print "Couldn't load soundfont", SF2
    sys.exit(1)

# SOUND

base = "C"
all_scales = ["ionian", "dorian", "phrygian", "lydian", "mixolydian", "aeolian", "locrian", "natural_minor", "harmonic_minor", "melodic_minor", "chromatic", "whole_note"]

scale = []
interval = 0

def set_sound():
    scale_name = random.choice(all_scales)
    global scale
    scale = eval("scales." + scale_name + "(base)")
    label = font.render("Scale: " + scale_name.replace("_", " ").capitalize(), 1, (255, 255, 255))
    screen.blit(label, (50, 100))

    tempo = random.randint(150, 300)
    global interval
    interval = float(60)/tempo
    label = font.render("Tempo: " + str(tempo), 1, (255, 255, 255))
    screen.blit(label, (50, 150))

    label = font.render("Make sure you throughly wash your hands :)", 1, (255, 255, 255))
    screen.blit(label, (50, 250))

# INPUT

def angle_from_vector(vector):
    return math.degrees(math.atan2(vector[1], vector[0]))

def hands_are_close(hands):
    d = distance(hands[0].palm_position, hands[1].palm_position)
    return len(hands) == 1 or (d < 150 and d > 0)

def motion(translation):
    return (translation[0]**2 + translation[1]**2 + translation[2]**2)**(0.5)

def distance(p1, p2):
    return motion(p1 - p2)

def handle_frame(frame, previous):
    if not frame.hands.empty:
        translation = frame.hands[0].translation(previous)
        angle = int(angle_from_vector((translation[1], translation[2])))
        if hands_are_close(frame.hands) and motion(translation) > 1:
            handle_num(angle)

def handle_key(key):
    max_number = 310
    handle_num(key)
    
def handle_mouse(motion):
    if motion[0] != 0:
        handle_num(int(angle_from_vector(motion)))

def handle_num(num):
    note = Note(scale[abs(num%len(scale))], determine_octave(num))
    fluidsynth.play_Note(note, 8, 100)

octave = 4
def determine_octave(num):
    global octave
    if num%3 == 0 and octave < 8:
        octave += 1
    if num%3 == 1 and octave > 2:
        octave -= 1
    return octave

# VISUAL

color = [0, 100, 100]
fader = pygame.Surface((800, 600))
fader.fill((0, 0, 0))
fader.set_alpha(20)
icon = pygame.Surface((50, 50))
def visualize_frame(frame):
    screen.blit(fader, (0, 0))
    for hand in frame.hands:
        if hands_are_close(frame.hands):
            color[0] = (color[0] + 20)%255
            color[1] = (color[1] + 10)%255
            color[2] += (255 - color[2])/100
        pygame.draw.circle(icon, color, (25, 25), 100)
        screen.blit(icon, (hand.palm_position[0]*2 + 400, 600 - hand.palm_position[1]*2))

# MESSAGES

def win_message():
    label = font.render("You have now thoroughly washed your hands!", 1, (255, 255, 255))
    screen.blit(label, (30, 400))

# GAME

def input(events):
    for event in events:
        """if event.type == KEYDOWN:
            handle_key(event.key)
        if event.type == MOUSEMOTION:
            handle_mouse(pygame.mouse.get_rel())"""
        if event.type == QUIT:
            sys.exit(0)

start = 0
previous = controller.frame()

def wait():
    while not hands_are_close(controller.frame().hands):
        fader.set_alpha(100)
        screen.blit(fader, (0, 0))
        time.sleep(interval)
        input(pygame.event.get())
    set_sound()
    global start, previous
    start = time.time()
    previous = controller.frame()

wait()

while True:
    current = controller.frame()
    handle_frame(current, previous)
    visualize_frame(current)
    previous = current
    input(pygame.event.get())
    time.sleep(interval)
    elapsed = time.time() - start
    if elapsed > 20:
        if elapsed < 23:
            win_message()
        else:
            screen.blit(fader, (0, 0))
    if time.time() - start > 30:
        wait()
    pygame.display.flip()
